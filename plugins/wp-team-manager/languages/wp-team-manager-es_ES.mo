��    &      L      |      |     }     �     �  
   �      �  
   �  #   �     
          *     9     G     V     r  d   ~     �     �               "     2  .   I     x     �     �     �  !   �     �          $  -   D  )   r  #   �  #   �     �     �  !     �  7     
     
     /
     I
  (   Z
     �
  &   �
     �
     �
     �
     �
               3  a   D     �     �     �     �     �       *        D     b     i     w     �     �     �     �  .   �  0         1     M     h     o     �   Add New Group Add more image Add or remove Groups All Groups Choose from the most used Groups Edit Group File has been successfully deleted. New Group Name No Groups found. Popular Groups Search Groups Select a color Separate Groups with commas Team Groups This plugin allows you to manage the members of your team or staff and display them using shortcode. Update Group Upload new files Upload new images Uploaded files Uploaded images WordPress Team Manager You don't have permission to delete this file. http://www.dynamicweblab.com/ maidul wp-team-managerAdd New  wp-team-managerAdd New Member wp-team-managerEdit Team Member  wp-team-managerGroup wp-team-managerGroups wp-team-managerNew Team Member wp-team-managerNo Team Member found in Trash wp-team-managerNot found any Team Member wp-team-managerParent Team Member: wp-team-managerSearch Team Members wp-team-managerTeam wp-team-managerTeam Member wp-team-managerView Team Members Project-Id-Version: WordPress Team Manager 1.1
Report-Msgid-Bugs-To: http://wordpress.org/tag/wp-team-manager
POT-Creation-Date: 2014-01-31 18:39:18+00:00
PO-Revision-Date: Thu Apr 02 2015 12:53:37 GMT+0200 (CEST)
Last-Translator: Angeles Broullón <thalionte@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: Spanish (Spain)
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Generator: Loco - https://localise.biz/
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: es_ES Añadir nuevo Grupo Añadir una imagen mas. Añadir o eliminar grupos Todos los Grupos Elegir grupo de entre los mas utilizados Editar Grupo El fichero ha sido borrado con éxito. Nombre del nuevo Grupo No se encontraron grupos Grupos Populares Buscar Grupos Eleccione un color. Separar Grupos con comas Grupos de Equipo Este plugin permite organizar los grupos de su equipo o staff y visualizarlos mediante shortcode. Actualizar Grupo Enviar nuevo fichero. Subir nueva imagen. Fichero enviado. Fichero subido. WordPress Team Manager No tiene permiso para borrar este fichero. http://www.dynamicweblab.com/ maidul añadir nuevo Añadir un nuevo miembro. Editar miembro del equipo Grupo Grupos Nuevo miembro del equipo No se encontró ningún miembro en la Papelera No se encontró ningún miembro para este equipo Miebro del equipo principal Buscar miembros del equipo Equipo Miembro del equipo Ver miembros del equipo 