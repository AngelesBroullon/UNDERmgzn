<?php
/*
Plugin Name: Social circle Widget
Plugin URI: https://github.com/AngelesBroullon/UNDERmagazine/plugins/social-circle-widget
Description: Displays a circle of social icons.
Author: Angeles Broullón
Version: 1.0
Author URI: http://mangelesbroullon.wordpress.com/acerca-de/

/* License
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
*/

add_action('wp_enqueue_scripts', 'add_social_circle_widget_css');

function add_social_circle_widget_css() {

	$social_circle_widget_myStyleUrl = plugins_url('style.css', __FILE__); // Respects SSL, Style.css is relative to the current file
	$social_circle_widget_myStyleFile = WP_PLUGIN_DIR . '/social-circle-widget/style.css';	

	if ( file_exists($social_circle_widget_myStyleFile) ) {
		wp_register_style('socialcirclewidgetcss', $social_circle_widget_myStyleUrl);
		wp_enqueue_style( 'socialcirclewidgetcss');
	}
}

function get_social_circle($facebook_url, $twitter_url, $google_plus_url, $instagram_url, $pinterest_url, $rss_url) {
// This is the main function of the plugin.

	$fb = '<a class="minifacebook" href="' . $facebook_url . '" title="Facebook"  target="_blank">Facebook</a>';
	$tw = '<a class="minitwitter" href="' . $twitter_url . '" title="Twitter"  target="_blank">Twitter</a>';
	$gp = '<a class="minigoogleplus" href="' . $google_plus_url . '" title="Google+"  target="_blank">Google+</a>';
	$ins = '<a class="miniinstagram" href="' . $instagram_url . '" title="Instagram"  target="_blank">Instagram</a>';
	$pin = '<a class="minipinterest" href="' . $pinterest_url . '" title="Pinterest"  target="_blank">Pinterest</a>';
	$rss = '<a class="minirss" href="' . $rss_url . '" title="RSS" target="_blank">RSS</a>';
	$content = '<div class="social-circle" align="center">    
		<table class="socialw_table" style=" width:140px; height:140px">
		<tbody>
			<tr>
    			<td></td><td>' . $fb . '</td><td></td><td>' . $tw . '</td><td></td>
			</tr>
			<tr>
    			<td></td><td></td><td></td><td></td><td></td>
			</tr>
			<tr>
    		<td>' . $ins . '</td><td></td><td></td><td></td><td>' . $pin . '</td>
			</tr>
			<tr>
    			<td></td><td></td><td></td><td></td><td></td>
			</tr>
			<tr>
    			<td></td><td>' . $gp . '</td><td></td><td>' . $rss . '</td>
			</tr>
		</tbody>
	</table></div>';
					
	return $content;
}

class Social_Circle_Widget extends WP_Widget {
  function Social_Circle_Widget() {
    $widget_ops = array('classname' => 'social_circle_widget', 'description' => 'A widget that displays a circle of social icons' );
    $this->WP_Widget('social_circle_widget', 'Social Circle Widget', $widget_ops);
  }

  function widget($args, $instance) {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;

    $title = empty($instance['title']) ? '&nbsp;' : apply_filters('widget_title', $instance['title']);
    $facebook_url = empty($instance['facebook_url']) ? '' : $instance['facebook_url'];
    $twitter_url = empty($instance['twitter_url']) ? '' : $instance['twitter_url'];
    $google_plus_url = empty($instance['google_plus_url']) ? '' : $instance['google_plus_url'];
    $instagram_url = empty($instance['instagram_url']) ? '' : $instance['instagram_url'];
    $pinterest_url = empty($instance['pinterest_url']) ? '' : $instance['pinterest_url'];
    $rss_url = empty($instance['rss_url']) ? '' : $instance['rss_url'];
    
 
    if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };

    if ( empty( $facebook_url ) ) { $facebook_url = ''; };
    if ( empty( $twitter_url ) ) { $twitter_url = ''; };
    if ( empty( $google_plus_url ) ) { $google_plus_url = ''; };
    if ( empty( $instagram_url ) ) { $instagram_url = ''; };
    if ( empty( $pinterest_url ) ) { $pinterest_url = ''; };
    if ( empty( $rss_url ) ) { $rss_url = ''; };

    echo get_social_circle($facebook_url, $twitter_url, $google_plus_url, $instagram_url, $pinterest_url, $rss_url); ?>

    	<div style="clear:both;"></div>
    
    	<?php

    echo $after_widget;
  }
 
  function update($new_instance, $old_instance) {
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['facebook_url'] = strip_tags($new_instance['facebook_url']);
    $instance['twitter_url'] = strip_tags($new_instance['twitter_url']);
    $instance['google_plus_url'] = strip_tags($new_instance['google_plus_url']);
    $instance['instagram_url'] = strip_tags($new_instance['instagram_url']);
    $instance['pinterest_url'] = strip_tags($new_instance['pinterest_url']);
    $instance['rss_url'] = strip_tags($new_instance['rss_url']);
 
    return $instance;
  }
 
  function form($instance) {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'facebook_url' => '', 'twitter_url' => '', 'google_plus_url' => '', 'instagram_url' => '', 'pinterest_url' => '', 'rss_url' => '' ));
    $title = strip_tags($instance['title']);
    $facebook_url = strip_tags($instance['facebook_url']);
    $twitter_url = strip_tags($instance['twitter_url']);
    $google_plus_url = strip_tags($instance['google_plus_url']);
    $instagram_url = strip_tags($instance['instagram_url']);
    $pinterest_url = strip_tags($instance['pinterest_url']);
    $rss_url = strip_tags($instance['rss_url']);
?>
      <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <br /><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
								    
      <p><label for="<?php echo $this->get_field_id('facebook_url'); ?>">Facebook URL: <br /><input class="widefat" id="<?php echo $this->get_field_id('facebook_url'); ?>" name="<?php echo $this->get_field_name('facebook_url'); ?>" type="text" value="<?php echo attribute_escape($facebook_url); ?>" /></label></p>

      <p><label for="<?php echo $this->get_field_id('twitter_url'); ?>">Twitter URL: <br /><input class="widefat" id="<?php echo $this->get_field_id('twitter_url'); ?>" name="<?php echo $this->get_field_name('twitter_url'); ?>" type="text" value="<?php echo attribute_escape($twitter_url); ?>" /></label></p>
      
      <p><label for="<?php echo $this->get_field_id('google_plus_url'); ?>">Google+ URL: <br /><input class="widefat" id="<?php echo $this->get_field_id('google_plus_url'); ?>" name="<?php echo $this->get_field_name('google_plus_url'); ?>" type="text" value="<?php echo attribute_escape($google_plus_url); ?>" /></label></p>
      
      <p><label for="<?php echo $this->get_field_id('instagram_url'); ?>">Instagram URL: <br /><input class="widefat" id="<?php echo $this->get_field_id('instagram_url'); ?>" name="<?php echo $this->get_field_name('instagram_url'); ?>" type="text" value="<?php echo attribute_escape($instagram_url); ?>" /></label></p>
      
      <p><label for="<?php echo $this->get_field_id('pinterest_url'); ?>">Pinterest URL: <br /><input class="widefat" id="<?php echo $this->get_field_id('pinterest_url'); ?>" name="<?php echo $this->get_field_name('pinterest_url'); ?>" type="text" value="<?php echo attribute_escape($pinterest_url); ?>" /></label></p>
      
      <p><label for="<?php echo $this->get_field_id('rss_url'); ?>">RSS URL: <br /><input class="widefat" id="<?php echo $this->get_field_id('rss_url'); ?>" name="<?php echo $this->get_field_name('rss_url'); ?>" type="text" value="<?php echo attribute_escape($rss_url); ?>" /></label></p>

<?php
																			}
}

// register_widget('Social_Circle_Widget');
add_action( 'widgets_init', create_function('', 'return register_widget("Social_Circle_Widget");') );

add_filter( 'wp_feed_cache_transient_lifetime', create_function('$a', 'return 600;') );

?>
