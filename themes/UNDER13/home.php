<?php get_header(); ?>
<?php $option =  get_option('trt_options'); ?>

<!--SLIDER-->
<?php if ( is_home() ) { ?>
<div id="slider_wrap">
    <div class="center">
        <div id="slides">
        <?php if( empty($option['trt_slider'])) { ?>
        <?php get_template_part('easyslider');?>
    	<?php }?>
        <?php if($option['trt_slider']== "Easyslider") { ?>
        <?php get_template_part('easyslider');?>
    	<?php }?>
        <?php if($option['trt_slider']== "Disable Slider") { ?>
    	<?php }?>
        
        </div>
    </div>
</div>
    	<?php }?>

<!--CONTENT-->
<div id="content">

    <div class="center">
        <?php get_template_part('layoutmain');?>
    </div>
</div>

<?php get_footer(); ?>