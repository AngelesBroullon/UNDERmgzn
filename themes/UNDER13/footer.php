<?php $option =  get_option('trt_options'); ?>
<!--MIDROW-->
<?php if($option["trt_diss_ftw"] == "1"){ ?><?php } else { ?>
<div id="midrow">
    <div class="widgets"><ul>          
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Midrow Widgets') ) : ?> 
        <?php endif; ?></ul>
    </div>
</div>
</div>
<?php }?>

<!--FOOTER-->
<div id="footer">
    <!-- COPYRIGHT -->
    <div id="site-generator">
        <div class="logo_foot" id= "logo_foot_under"></div>
        <div class="text_foot" id= "text_foot_under">
        Algunos derechos reservados: <a href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.es_CO" rel="license"><img style="border-width: 0;" src="http://i.creativecommons.org/l/by-nc-nd/3.0/80x15.png" alt="Licencia Creative Commons" /></a> - <a class="" href="http://www.undermgzn.com/aviso-legal" title="Aviso legal" >Aviso legal</a> - <a class="" href="http://www.undermgzn.com/contacto" title="Contacta con UNDER mgzn" >Contacto</a></div>
    </div>    

    <div class="widgets-container">
       <div class="widgets"><ul>          
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widgets') ) : ?> 
            <?php endif; ?></ul>
        </div>
    </div>

</div>
<?php wp_footer(); ?>

</body>
</html>

