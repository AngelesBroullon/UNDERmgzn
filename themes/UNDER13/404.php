<?php get_header(); ?>

<!--SLIDER-->
<?php if ( is_home() ) { ?>
<div id="slider_wrap">
    <div class="center">
    <div id="slides"><?php get_template_part('easyslider'); ?></div>
    </div>
</div>
    	<?php }?>

<!--CONTENT-->
<div id="content">

<div class="center">
    <div id="content_wrap" class="error_page">
            <!--404 Error-->
            <div class="fourofour"><label><a>404</a></label></div>
            <div class="post">
            <h2><?php _e('Página no encontrada', 'triton'); ?></h2>
            <div class="error_msg">
            <p><label><?php _e('El servidor no pudo encontrar la página solicitada. El archivo ha sido movido o borrado, o puede que usted haya introducido una URL o nombre de documento incorrecto. Compruebe la URL, y si ha  cometido un error corríjalo y pruebe de nuevo. Si no funciona puede probar nuestra opción de búsqueda.', 'triton'); ?></label></p>
            <!--<?php get_search_form(); ?>-->
            </div>
        </div>     
                
            </div> 
</div>
</div>

<?php get_footer(); ?>